﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter10
{
   [Serializable]
  public  class Teacher:Person
    {
       
        public Teacher() { }
        public Teacher(string name, int age, string sex, string subject)
            : base(name,age, sex)
        {
            this.Subject = subject;
        }
        public string Subject { get; set; }
        public override string SayHello()
        {
            return string.Format("大家好，我是老师{0}，所教科目为{1}", Name, Subject);
          
        }
    }
}

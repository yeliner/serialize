﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Chapter10
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnS_Click(object sender, EventArgs e)
        {//二进制序列化
            Student stu=new Student ("球球",19,"女",90);
            FileStream fs = new FileStream(@"..\Chapter10\stu.txt",FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs,stu);
            fs.Close();
            MessageBox.Show("二进制序列化成功!");

        }

        private void btnD_Click(object sender, EventArgs e)
        {
            //二进制反序列化
            FileStream fs = new FileStream(@"..\Chapter10\stu.txt", FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            Student stu =(Student)bf.Deserialize(fs);
            fs.Close();
            MessageBox.Show(stu.SayHello());
        }

        private void btnXmlS_Click(object sender, EventArgs e)
        {
            //Xml序列化
            Teacher tea = new Teacher("羊羊", 19, "男", "数学");
            FileStream fs = new FileStream(@"..\Chapter10\tea.xml", FileMode.Create);
            XmlSerializer xs = new XmlSerializer(typeof(Teacher));
            xs.Serialize(fs, tea);
            fs.Close();
            MessageBox.Show("Xml序列化成功!");
        }

        private void btnXmlD_Click(object sender, EventArgs e)
        {
            //Xml反序列化
         
            FileStream fs = new FileStream(@"..\Chapter10\tea.xml", FileMode.Open);
            XmlSerializer xs = new XmlSerializer(typeof(Teacher));
            Teacher tea = (Teacher)xs.Deserialize(fs);
            fs.Close();
            MessageBox.Show(tea.SayHello());
        }

        private void btnSList_Click(object sender, EventArgs e)
        {
            //二进制集合序列化
            List<Person> ps = new List<Person>();
            Teacher tea = new Teacher("AA", 19, "男", "数学");
            Student stu = new Student("aa", 19, "女", 90);
            ps.Add(tea);
            ps.Add(stu);

            FileStream fs = new FileStream(@"..\Chapter10\stuList.txt", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, ps);
            fs.Close();
            MessageBox.Show("二进制集合序列化成功!");
        }

        private void btnDList_Click(object sender, EventArgs e)
        {
            //二进制集合反序列化
            FileStream fs = new FileStream(@"..\Chapter10\stuList.txt", FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            List<Person> ps = (List<Person>)bf.Deserialize(fs);
            fs.Close();
            foreach (Person item in ps)
            {
                MessageBox.Show(item.SayHello());
            }
         
        }

        private void btnXmlSList_Click(object sender, EventArgs e)
        {
            //Xml集合序列化
            List<Person> ps = new List<Person>();
            Teacher tea = new Teacher("BB", 19, "男", "数学");
            Student stu = new Student("bb", 19, "女", 90);
            ps.Add(tea);
            ps.Add(stu);
            FileStream fs = new FileStream(@"..\Chapter10\teaList.xml", FileMode.Create);
            XmlSerializer xs = new XmlSerializer(typeof(PersonList));
            PersonList pl = new PersonList();
            pl.ps = ps;
            xs.Serialize(fs, pl);
            fs.Close();
            MessageBox.Show("Xml集合序列化成功!");
            


        }

        private void btnXmlDList_Click(object sender, EventArgs e)
        {

            //Xml集合反序列化

            FileStream fs = new FileStream(@"..\Chapter10\teaList.xml", FileMode.Open);
            XmlSerializer xs = new XmlSerializer(typeof(PersonList));

          PersonList pl=(PersonList)xs.Deserialize(fs);

            fs.Close();

            foreach (Person  item in pl.ps)
            {
                MessageBox.Show(item.SayHello());
            }
           
        }

    }
}

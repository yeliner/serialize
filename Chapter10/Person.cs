﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chapter10
{
    [Serializable]
   public   class Person
    {
        public Person() { }
        public Person(string name, int age, string sex) { this.Name = name; this.Age = age; this.Sex = sex; }
        public string Name {get;set; }
        public int Age {get;set; }
        public string Sex { get; set; }
        public virtual string SayHello() {
            return "";
        }

    }
}

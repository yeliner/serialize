﻿namespace Chapter10
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnS = new System.Windows.Forms.Button();
            this.btnD = new System.Windows.Forms.Button();
            this.btnXmlS = new System.Windows.Forms.Button();
            this.btnXmlD = new System.Windows.Forms.Button();
            this.btnSList = new System.Windows.Forms.Button();
            this.btnDList = new System.Windows.Forms.Button();
            this.btnXmlSList = new System.Windows.Forms.Button();
            this.btnXmlDList = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnS
            // 
            this.btnS.Location = new System.Drawing.Point(33, 41);
            this.btnS.Name = "btnS";
            this.btnS.Size = new System.Drawing.Size(109, 23);
            this.btnS.TabIndex = 0;
            this.btnS.Text = "二进制序列化";
            this.btnS.UseVisualStyleBackColor = true;
            this.btnS.Click += new System.EventHandler(this.btnS_Click);
            // 
            // btnD
            // 
            this.btnD.Location = new System.Drawing.Point(188, 41);
            this.btnD.Name = "btnD";
            this.btnD.Size = new System.Drawing.Size(125, 23);
            this.btnD.TabIndex = 1;
            this.btnD.Text = "二进制反序列化";
            this.btnD.UseVisualStyleBackColor = true;
            this.btnD.Click += new System.EventHandler(this.btnD_Click);
            // 
            // btnXmlS
            // 
            this.btnXmlS.Location = new System.Drawing.Point(33, 88);
            this.btnXmlS.Name = "btnXmlS";
            this.btnXmlS.Size = new System.Drawing.Size(109, 23);
            this.btnXmlS.TabIndex = 2;
            this.btnXmlS.Text = "Xml序列化";
            this.btnXmlS.UseVisualStyleBackColor = true;
            this.btnXmlS.Click += new System.EventHandler(this.btnXmlS_Click);
            // 
            // btnXmlD
            // 
            this.btnXmlD.Location = new System.Drawing.Point(188, 88);
            this.btnXmlD.Name = "btnXmlD";
            this.btnXmlD.Size = new System.Drawing.Size(125, 23);
            this.btnXmlD.TabIndex = 3;
            this.btnXmlD.Text = "Xml序列化";
            this.btnXmlD.UseVisualStyleBackColor = true;
            this.btnXmlD.Click += new System.EventHandler(this.btnXmlD_Click);
            // 
            // btnSList
            // 
            this.btnSList.Location = new System.Drawing.Point(33, 133);
            this.btnSList.Name = "btnSList";
            this.btnSList.Size = new System.Drawing.Size(109, 23);
            this.btnSList.TabIndex = 4;
            this.btnSList.Text = "二进制集合序列化";
            this.btnSList.UseVisualStyleBackColor = true;
            this.btnSList.Click += new System.EventHandler(this.btnSList_Click);
            // 
            // btnDList
            // 
            this.btnDList.Location = new System.Drawing.Point(188, 133);
            this.btnDList.Name = "btnDList";
            this.btnDList.Size = new System.Drawing.Size(125, 23);
            this.btnDList.TabIndex = 5;
            this.btnDList.Text = "二进制集合序列化";
            this.btnDList.UseVisualStyleBackColor = true;
            this.btnDList.Click += new System.EventHandler(this.btnDList_Click);
            // 
            // btnXmlSList
            // 
            this.btnXmlSList.Location = new System.Drawing.Point(33, 178);
            this.btnXmlSList.Name = "btnXmlSList";
            this.btnXmlSList.Size = new System.Drawing.Size(109, 23);
            this.btnXmlSList.TabIndex = 6;
            this.btnXmlSList.Text = "Xml集合序列化";
            this.btnXmlSList.UseVisualStyleBackColor = true;
            this.btnXmlSList.Click += new System.EventHandler(this.btnXmlSList_Click);
            // 
            // btnXmlDList
            // 
            this.btnXmlDList.Location = new System.Drawing.Point(188, 178);
            this.btnXmlDList.Name = "btnXmlDList";
            this.btnXmlDList.Size = new System.Drawing.Size(125, 23);
            this.btnXmlDList.TabIndex = 7;
            this.btnXmlDList.Text = "Xml集合序列化";
            this.btnXmlDList.UseVisualStyleBackColor = true;
            this.btnXmlDList.Click += new System.EventHandler(this.btnXmlDList_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 262);
            this.Controls.Add(this.btnXmlDList);
            this.Controls.Add(this.btnXmlSList);
            this.Controls.Add(this.btnDList);
            this.Controls.Add(this.btnSList);
            this.Controls.Add(this.btnXmlD);
            this.Controls.Add(this.btnXmlS);
            this.Controls.Add(this.btnD);
            this.Controls.Add(this.btnS);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnS;
        private System.Windows.Forms.Button btnD;
        private System.Windows.Forms.Button btnXmlS;
        private System.Windows.Forms.Button btnXmlD;
        private System.Windows.Forms.Button btnSList;
        private System.Windows.Forms.Button btnDList;
        private System.Windows.Forms.Button btnXmlSList;
        private System.Windows.Forms.Button btnXmlDList;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace Chapter10
{
    public  class PersonList
    {
  [XmlArrayItem(typeof(Person))]
  [XmlArrayItem(typeof(Student))]
  [XmlArrayItem(typeof(Teacher))]
      public  List<Person> ps = new List<Person>();

    }
}

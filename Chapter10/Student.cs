﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter10
{
        [Serializable]
    public class Student : Person

    {
        public Student() { }
        public Student(string name,int age,string sex, int score):base(name,age,sex){
            this.Score = score;
        }
        public int Score { get; set; }
        public override string SayHello() {
            return string.Format("大家好，我是学生{0}，这次考了{1}分",Name,Score);
        }

    }
}
